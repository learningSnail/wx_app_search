$(function () {
    //走马灯
    $(".font_inner li:eq(0)").clone(true).appendTo($(".font_inner"));//克隆第一个放到最后(实现无缝滚动)
    var liHeight = $(".swiper_wrap").height();//一个li的高度
    //获取li的总高度再减去一个li的高度(再减一个Li是因为克隆了多出了一个Li的高度)
    var totalHeight = ($(".font_inner li").length * $(".font_inner li").eq(0).height()) - liHeight;
    $(".font_inner").height(totalHeight);//给ul赋值高度
    var index = 0;
    var autoTimer = 0;//全局变量目的实现左右点击同步
    var clickEndFlag = true; //设置每张走完才能再点击

    function tab() {
        $(".font_inner").stop().animate({
            top: -index * liHeight
        }, 400, function () {
            clickEndFlag = true;//图片走完才会true
            if (index == $(".font_inner li").length - 1) {
                $(".font_inner").css({top: 0});
                index = 0;
            }
        })
    }
    function next() {
        index++;
        if (index > $(".font_inner li").length - 1) {//判断index为最后一个Li时index为0
            index = 0;
        }
        tab();
    }
    //自动轮播
    autoTimer = setInterval(next, 1000);
    $(".font_inner a").hover(function () {
        clearInterval(autoTimer);
    }, function () {
        autoTimer = setInterval(next, 1000);
    });
});

function nickCheck() {
    var phone = localStorage.getItem("phone");
    var myname = localStorage.getItem("name");
    var appName = $("#appName").val();
    var nickName = localStorage.getItem("nickname");
    if(appName === nickName){
        layer.open({
            content: '亲，换个词查一查！',
            skin: 'msg',
            time: 2
        });
    }else if (appName.trim() === '') {
        $("#appName").focus();
    }else if(phone && myname){
        $("#tel").val(phone);
        $("#myname").val(myname);
        btn_ok();
    }else {
        $(".cover").show();
    }
}
function btn_Cancel() {
    $(".cover").css('display', 'none');
}
function btn_ok() {
    var tel = $("#tel").val();
    var myname = $("#myname").val();
    var appName = $("#appName").val();
    var mobilevalid = /^(0|86|17951)?(13[0-9]|15[012356789]|17[0678]|18[0-9]|14[57])[0-9]{8}$/;
    if (myname.trim() === '') {
        layer.open({
            content: '请正确填写您的姓名',
            skin: 'msg',
            time: 2
        });
        $("#myname").focus();
        return false;
    }else if(!mobilevalid.test(tel)) {
        layer.open({
            content: '请填写正确的手机号',
            skin: 'msg',
            time: 2
        });
        $("#tel").focus();
        return false;
    }else {
        localStorage.phone = tel;
        localStorage.name = myname;
        localStorage.nickname = appName;
        doCheck(appName);
    }
    btn_Cancel();
}

function doCheck(nickname) {
    var msgStr = "";
    //提交查询
    $.ajax({
        type: 'POST',
        url: 'core/check_nickname.php',
        dataType: 'json',
        data: {
            tel : localStorage.getItem("phone")?localStorage.getItem("phone"):'',
            name : localStorage.getItem("name")?localStorage.getItem("name"):'',
            compay : localStorage.getItem("compay")?localStorage.getItem("compay"):'',
            nickname: nickname,
            random: Math.random()
        },
        success: function (res) {
            $("#msgbox").attr("class", "warning");
            $("#btn-send").css('display', 'none');
            $(".xx-result").css('display', 'block');
            var code = -1;
            if (res && res.base_resp.ret == 0) {
                code = res.check_nickname_resp.ret;
                //alert(code);

                //插入统计
                _vds.push(['setCS1', 'user_name', localStorage.getItem("name")?localStorage.getItem("name"):'']);
                _vds.push(['setCS2', 'user_tel', localStorage.getItem("phone")?localStorage.getItem("phone"):'']);
                _vds.push(['setCS3', 'company_name', localStorage.getItem("compay")?localStorage.getItem("compay"):'']);
                _vds.push(['setCS4', 'check_nickname', nickname]);
                _vds.push(['setCS5', 'check_status', code]);

                switch (code) {
                    case 0:
                        msgStr = '你的名字可以使用!';
                        $("#msgbox").attr("class", "success");
                        $("#btn-send").css('display', 'block');
                        break;
                    case 1004:
                        msgStr = '名称与已有帐号名称重复。请提交与订阅号、服务号、小程序不重复的帐号名称。';
                        break;
                    case 200003 :
                        msgStr = '好累，服务器罢工了！';
                        break;
                    case 260003 :
                        msgStr = '该名称与已有小程序名称重复，请重新提交一个新的名称。';
                        break;
                    case 260007:
                        msgStr = '该名称与已有多个公众号名称重复，需与该帐号相同主体才可申请。';
                        break;
                    case 260008 :
                        msgStr = '该名称与已有公众号名称重复，需与该小程序帐号相同主体才可申请。';
                        break;
                    case 260009:
                        msgStr = '该名称与已有多个公众号名称重复，暂不支持申请，请重新提交一个新的名称。';
                        break;
                    case 260010:
                        msgStr = '该名称与已有多个公众号名称重复，需与该帐号相同主体才可申请。';
                        break;
                    case 200013:
                        msgStr = '提交次数过于频繁，请稍后再试';
                        break;
                    case 210041:
                        msgStr = '名称长度为4-30个字符，不能含有特殊字符及“微信”等保留字';
                        break;
                    case 210050:
                        msgStr = '名称不能与已有公众帐号的微信号重复';
                        break;
                    case 210044:
                        msgStr = '名称不能与已有公众帐号的微信号重复';
                        break;
                    case 210046:
                        msgStr = '该名称在侵权投诉保护期，暂不支持申请，请重新提交一个新的名称';
                        break;
                    case 65201:
                        msgStr = '不能使用该名称';
                        break;
                    case 211003:
                        msgStr = '名称正在2天保护期中，暂不能申请使用；你可在保护期满后重新申请使用该名称';
                        break;
                    case 20 :
                        msgStr = '该名称需要提交相应资料进行审核。';
                        break;
                    case -41:
                        msgStr = '公众账号名称只允许含有中文、英文大小写、数字，长度为4-30个字符';
                        break;
                    case 65201:
                        msgStr = '不能使用该名称';
                        break;
                    default:
                        msgStr = '系统错误，请稍后重试！';
                }
            } else {
                msgStr = '慢点，微信说你点太快了！(' + res.base_resp.ret + ')';
            }
            localStorage.nickstatus = code;
            $("#result").html(nickname);
            $("#msgbox").html(msgStr);
            //$("#appName").focus();
        }
    });
}

function doSend() {
    localStorage.nickname = $("#appName").val();
    window.location.href = 'send.php';
}