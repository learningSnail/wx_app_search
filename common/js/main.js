$(function () {
    function wx() {
        var $this = {};
        $this.appName = document.getElementById("appName");
        $this.searchBtn = document.getElementsByTagName('button')[0];

        $this.verify = function () {
            var appName = $this.appName.value;
            var appNameStore = localStorage.getItem('appname');
            if (appName === appNameStore) {
                layer.open({
                    content: '亲，换个词查一查！',
                    skin: 'msg',
                    time: 2
                });
            } else if (appName == undefined || appName.trim() === '') {
                $("#appName").focus();
            } else {
                $this.doPost();
            }
        }

        $this.doPost = function () {
            var message = "";
            $.ajax({
                type: 'POST',
                url: './search.php',
                dataType: 'json',
                data: {
                    nickname: $this.appName.value,
                    random: Math.random()
                },
                success: function (res) {
                    $("#msgbox").attr("class", "warning");
                    $("#btn-send").css('display', 'none');
                    $(".xx-result").css('display', 'block');
                    var code = -1;
                    if (res && res.base_resp.ret == 0) {
                        code = res.check_nickname_resp.ret;
                        //alert(code);
                        switch (code) {
                            case 0:
                                msgStr = '你的名字可以使用!';
                                $("#msgbox").attr("class", "success");
                                $("#btn-send").css('display', 'block');
                                break;
                            case 1004:
                                msgStr = '名称与已有帐号名称重复。请提交与订阅号、服务号、小程序不重复的帐号名称。';
                                break;
                            case 200003 :
                                msgStr = '好累，服务器罢工了！';
                                break;
                            case 260003 :
                                msgStr = '该名称与已有小程序名称重复，请重新提交一个新的名称。';
                                break;
                            case 260007:
                                msgStr = '该名称与已有多个公众号名称重复，需与该帐号相同主体才可申请。';
                                break;
                            case 260008 :
                                msgStr = '该名称与已有公众号名称重复，需与该小程序帐号相同主体才可申请。';
                                break;
                            case 260009:
                                msgStr = '该名称与已有多个公众号名称重复，暂不支持申请，请重新提交一个新的名称。';
                                break;
                            case 260010:
                                msgStr = '该名称与已有多个公众号名称重复，需与该帐号相同主体才可申请。';
                                break;
                            case 200013:
                                msgStr = '提交次数过于频繁，请稍后再试';
                                break;
                            case 210041:
                                msgStr = '名称长度为4-30个字符，不能含有特殊字符及“微信”等保留字';
                                break;
                            case 210050:
                                msgStr = '名称不能与已有公众帐号的微信号重复';
                                break;
                            case 210044:
                                msgStr = '名称不能与已有公众帐号的微信号重复';
                                break;
                            case 210046:
                                msgStr = '该名称在侵权投诉保护期，暂不支持申请，请重新提交一个新的名称';
                                break;
                            case 65201:
                                msgStr = '不能使用该名称';
                                break;
                            case 211003:
                                msgStr = '名称正在2天保护期中，暂不能申请使用；你可在保护期满后重新申请使用该名称';
                                break;
                            case 20 :
                                msgStr = '该名称需要提交相应资料进行审核。';
                                break;
                            case -41:
                                msgStr = '公众账号名称只允许含有中文、英文大小写、数字，长度为4-30个字符';
                                break;
                            case 65201:
                                msgStr = '不能使用该名称';
                                break;
                            default:
                                msgStr = '系统错误，请稍后重试！';
                        }
                    } else {
                        msgStr = '慢点，微信说你点太快了！(' + res.base_resp.ret + ')';
                    }
                    localStorage.nickstatus = code;
                    $("#result").html($this.appName.value);
                    $("#msgbox").html(msgStr);
                    //$("#appName").focus();
                }
            })
        }

        $this.init = function () {
            $this.searchBtn.onclick = $this.verify
        }

        $this.init();
    }
    wx();
})

