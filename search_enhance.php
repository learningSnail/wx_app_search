<?php
/**
 * Created by PhpStorm.
 * User: lehong
 * Date: 2017/8/25
 * Time: 17:44
 */
//防盗链
$header = [
    "Accept" => "application/json, text/javascript, */*; q=0.01",
    "Accept-Encoding" => "gzip, deflate",
    "Accept-Language" => "zh-CN,zh;q=0.8",
    "Connection" => "keep-alive",
    "Content-Type" => "application/x-www-form-urlencoded; charset=UTF-8",
    "Cookie" => "gr_user_id=9ae22b74-c863-468b-99df-29f7277a42be; gr_session_id_82c7060d048a375f=003cecc3-1a51-4af6-a1b5-1d5666e1c6f7; gr_cs1_003cecc3-1a51-4af6-a1b5-1d5666e1c6f7=user_name%3A%E9%9D%A0%E3%80%82%E3%80%82%E3%80%82%E3%80%82",
    "Host" => "n.job998.net",
    "Origin" => "http://n.job998.net",
    "Referer" => "http://n.job998.net/check.php",
    "User-Agent" => "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.90 Safari/537.36",
    "X-Requested-With" => "XMLHttpRequest"
];

$postData = [
    "nickname" => $_POST['nickname'],
];

$ch  = curl_init();
curl_setopt($ch,CURLOPT_URL,'http://n.job998.net/core/check_nickname.php');
curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,0);
curl_setopt($ch,CURLOPT_SSL_VERIFYHOST,0);
curl_setopt($ch,CURLOPT_POST,1);
curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
curl_setopt($ch,CURLOPT_POSTFIELDS,$postData);

$data = curl_exec($ch);
echo $data;
